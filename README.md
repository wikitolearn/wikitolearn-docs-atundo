# wikitolearn-docs

## Synopsis
This repository holds the WikiToLearn technical documentation. It uses MkDocs to generate a static HTML website.

The documentation go through architectural and infrastructure concepts, repositories structure and contributing guidelines.
This repository does not contain specific documentation about a single service or any WikiToLearn sub-project.

## Development
We use Docker to speed-up development and setup the environment without any dependency issues.

### Minimum requirements
* Docker Engine 18.02.0+
* Docker Compose 1.20.0+

### How to run
It is advisable to run using the `docker-compose.yml` file provided.

To run the MkDocs development server execute:

```
$ docker-compose up
```

The `.md` source files are mounted as a Docker `bind` volume to exploit development server auto-refresh.

The documentation website will be available at: `http://localhost:$SERVICE_PORT`, where `$SERVICE_PORT` is the value specified within `.env` file.

To build the site to deploy it somewhere execute the command below:

```
$ docker-compose build
$ docker-compose run --entrypoint "mkdocs build --clean" -u $(id -u):$(id -g) docs
```

## Versioning
We use [SemVer](http://semver.org/) for versioning.

## License
This project is licensed under the CC-BY-SA 4.0. See the [LICENSE](LICENSE) file for details.
