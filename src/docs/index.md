# WikiToLearn Documentation

Welcome to the WikiToLearn technical documentation!

---

## Overview

This the WikiToLearn home for developers. You will find a technical overview about the WikiToLearn architecture and infrastructure.

We suggest to start from the [architecture description](architecture.md) which lead you through architectural concepts.

Then, read the [Infrastructure page](infrastructure.md) if you are interested in how our infrastructure is managed.

Finally, the [contribution guide](contributing.md) will help you out to dig into code and became a WikiToLearn hacker.

Enjoy!
